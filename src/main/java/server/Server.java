package server;

import control.State;
import log.Log;
import model.User;

import java.util.ArrayList;

public class Server {

    private State state = new State();

    public static void main(String[] args) {

        //The first commandline argument is used as log level
        if (args.length > 0) Log.setLogLevel(args[0]);
        new Server();
    }

    public Server() {

        //Open a new server connection
        new NetworkManager(NetworkCons.SERVER_PORT, connection -> new User(connection, state));
    }
}