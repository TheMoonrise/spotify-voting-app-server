package server;

import log.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;

//Handles the byte streams to a sings client
public class ClientConnection extends Thread {

    private Socket socket;

    private BufferedReader reader;
    private BufferedWriter writer;

    //Whether the connection to the client is to be kept open
    private boolean isOpen = true;

    //Buffer list for storing messages before a listener is connected
    private ArrayList<String> messageBuffer = new ArrayList<>();

    //The listener receiving incoming messages
    private Listener listener;

    //Listener interface for handling incoming messages
    public interface Listener {

        void onMessageReceived(String message);
    }


    public ClientConnection(Socket socket) {

        this.socket = socket;

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            start();

            Log.i(this, "client connection opened to " + socket.getInetAddress());

        } catch (Exception e) {
            Log.e(this, "opening client connection failed");
        }
    }

    @Override
    public void run() {

        try {
            while (isOpen) {

                String line = reader.readLine();
                if (line != null) acceptMessage(line);
                else break;
            }

        } catch (Exception e) {
            //Left empty on purpose
        }

        try {
            reader.close();
            writer.close();
            socket.close();

        } catch (Exception e) {
            //Left empty on purpose
        }

        isOpen = false;
        Log.w(this, "Connection to client " + socket.getInetAddress() + " closed");
    }

    //Handles incoming messages
    private void acceptMessage(String message) {

        Log.m(this, "received message from client: " + message);

        if (listener == null) messageBuffer.add(message);
        else listener.onMessageReceived(message);
    }

    //Sends messages to the server
    //Returns whether the message was delivered successfully
    public boolean sendMessage(String message) {

        if (!isOpen) return false;

        try {
            writer.write(message);
            writer.newLine();
            writer.flush();

            Log.m(this, "sending message to client: " + message);

        } catch (Exception e) {
            Log.w(this, "sending message to server failed");

            isOpen = false;
            return false;
        }

        return true;
    }

    //Sets a new listener and send all missed messages
    public void setListener(Listener listener) {

        this.listener = listener;

        for (int i = messageBuffer.size() - 1; i >= 0; i--) listener.onMessageReceived(messageBuffer.get(i));
        messageBuffer.clear();
    }

    //Closes the connection to the client
    public void close() {

        try {
            isOpen = false;
            socket.close();

        } catch (Exception e) {
            Log.e("ServerConnection", "closing connection to client failed");
        }
    }

    //Returns whether the client connection is open
    public boolean isOpen() {
        return isOpen;
    }
}
