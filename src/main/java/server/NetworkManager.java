package server;

import log.Log;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

//Handles the server socket  and manages client connections
public class NetworkManager extends Thread {

    //The port the server socket is opened on
    private final int port;

    //Whether the server is running
    private boolean isOpen = true;

    //The socket of the active server
    private ServerSocket serverSocket;

    //The listener to receive new client connections
    private Listener listener;

    //Listener interface for handling new client connections
    public interface Listener {

        void onClientConnected(ClientConnection connection);
    }


    public NetworkManager(int port, Listener listener) {

        this.port = port;
        this.listener = listener;

        start();
    }

    @Override
    public void run() {

        while (isOpen) {

            try {
                serverSocket = new ServerSocket(port);
                Log.i(this, "server opened at " + InetAddress.getLocalHost().getHostAddress() + " on port " + port);

                while (isOpen) {
                    Socket socket = serverSocket.accept();
                    listener.onClientConnected(new ClientConnection(socket));
                }

            } catch (Exception e) {
                Log.w(this, "server interrupted." + (isOpen ? " restarting..." : ""));

                try {
                    if (serverSocket != null) serverSocket.close();

                } catch (Exception e2) {
                    //Left blank on purpose
                }
            }
        }

        Log.i(this, "server closed");
    }

    //Closes the server
    public void close() {

        try {
            isOpen = false;
            if (serverSocket != null) serverSocket.close();

        } catch (Exception e) {
            Log.w(this, "closing server failed. server might already be closed");
        }
    }
}
