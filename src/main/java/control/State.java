package control;

import JSON.JsonBuilder;
import model.Event;
import model.EventSettings;
import model.SphericalCoordinates;
import model.User;
import server.ClientConnection;

import java.util.ArrayList;

public class State {

    //List of all users on the server
    private ArrayList<User> connectedUser = new ArrayList<>();

    // List of all client connections on the server
    private ArrayList<ClientConnection> sessions = new ArrayList<>();

    //List of all active events
    private ArrayList<Event> events = new ArrayList<>();

    private long eventIdCounter;

    public ArrayList<User> getConnectedUser() {
        return connectedUser;
    }
    public ArrayList<ClientConnection> getSessions() {
        return sessions;
    }

    public void setSessions(ArrayList<ClientConnection> sessions) {
        this.sessions = sessions;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }


    //Creates a new event
    public Event createEvent(User host, String eventName, String password, SphericalCoordinates eventPosition, EventSettings settings) {
        String eventId= String.valueOf(eventIdCounter);
        eventIdCounter++;
        Event event = new Event(host, eventName, eventId, password, eventPosition, settings);

        events.add(event);
        event.addUser(host);
        host.setEvent(event);
        return event;
    }

    //Returns the event with the provided id
    public Event eventWithId(String eventId) {
        for (Event e : events) {
            if (e.getEventId().equalsIgnoreCase(eventId)) {
                return e;
            }
        }
        return null;
    }

    //Returns all events within a given radius
    public Event[] eventsWithinDistance(SphericalCoordinates center, double radius) {
        ArrayList<Event> eventsWithinDistance = new ArrayList<>();

        for (Event e : events) if (e.getEventPosition().distance(center) <= radius) eventsWithinDistance.add(e);
        return eventsWithinDistance.toArray(new Event[0]);
    }

    //returns a specific event from the List with all events
    public Event getEvent(Event event2) {
        for (Event event : events) {
            if (event.equals(event2)) {
                return event;
            }
        }
        return null;
    }

    //checks if User has already been connected before
    //if yes ClientConnection is changed to the new one
    public boolean checkIfIDinEvent(String id, User newUser) {
        ClientConnection newConnection= newUser.getConnection();
        for (Event event : events) {
            for (User oldUser : event.getGuests()) {
                if (oldUser.getUserId().equalsIgnoreCase(id)) {
                    oldUser.setConnection(newConnection);
                    oldUser.setAlreadyConnected(true);
                    newUser.setState(oldUser.getState());
                    newUser.setEvent(oldUser.getEvent());
                    newUser.setUserId(oldUser.getUserId());
                    newUser.setOwnEvent(oldUser.getOwnEvent());
                    return true;
                }
            }
        }
        return false;
    }

    public void deleteEvent( String eventId){
        ArrayList<Event> removedEvents = new ArrayList<>();
        for (Event event : events){
            if (event.getEventId().equalsIgnoreCase(eventId)){
                removedEvents.add(event);
                for (User user: event.getGuests()){
                    user.getConnection().sendMessage(JsonBuilder.eventEnded(event));
                }
            }
        }
        events.removeAll(removedEvents);
    }
}
