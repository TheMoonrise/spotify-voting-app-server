package cons;

public abstract class Cons {

    //The maximum distance an event can be accessed from in meters
    public static final double EVENT_ACCESS_DISTANCE = 350;
}
