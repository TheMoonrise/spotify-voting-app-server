package tests;

import JSON.JsonInterpreter;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import control.State;
import model.*;
import org.junit.Test;
import server.ClientConnection;
import server.Server;

import java.net.Socket;

/**
 * Created by sophiasigethy on 15.12.18.
 */
public class communication {

    @Test
    public void conmmunicate() throws Exception {

        State state = new State();
        Socket socket = null;
        Server server = new Server();
        ClientConnection clientConnection = new ClientConnection(socket);
        User user = new User(clientConnection, state);
        user.setUserId("abc");

        JsonInterpreter jsonInterpreter = new JsonInterpreter(user, state);


        JsonObject json = new JsonObject();
        json.addProperty("type", "identifyClient");
        json.addProperty("error", false);
        JsonObject data = new JsonObject();
        data.addProperty("userId", user.getUserId());
        json.add("data", data);


        Event event1 = new Event(user, "event1", user.getUserId(), "", new SphericalCoordinates(1.0,1.0), new EventSettings(true, (long)1.0,(long)1.0, null, true));
        Event event2 = new Event(user, "event2", user.getUserId(), "", new SphericalCoordinates(1.0,1.0), new EventSettings(true, (long)1.0,(long)1.0, null, true));

        User user2= new User(clientConnection, state);
        user2.setUserId("def");
        User user3= new User(clientConnection, state);
        user3.setUserId("ghi");

        event1.getGuests().add(user2);
        event1.getGuests().add(user3);

        state.getEvents().add(event1);
        state.getEvents().add(event2);

        //checks if new user is added correctly
       // jsonInterpreter.handle(json);


        //checks if user who has been connected before is recognized
        User user4= new User(clientConnection, state);
        user4.setEvent(event1);
        user4.setPoints(5);
        user4.setUserId("abc");
        event1.addUser(user4);

        jsonInterpreter.handle(json);

        JsonObject j = new JsonObject();
        j.addProperty("type", "getPlaylist");
        j.addProperty("error", false);
        JsonObject d = new JsonObject();
        j.add("data", d);

        //checks Jsonarray with objects
        Song song1= new Song("1", user.getUserId(),1);
        Song song2= new Song("2", user.getUserId(),1);
        Song song3= new Song("3", user.getUserId(), 1);
        event1.addSongToPlaylist(song1);
        event1.addSongToPlaylist(song2);
        event1.addSongToPlaylist(song3);
        String[] art= {"art1", "art2"};
        song1.setArtists(art);
        song2.setArtists(art);
        song3.setArtists(art);
        jsonInterpreter.setUser(user);
        user.setEvent(event1);
        jsonInterpreter.handle(j);


        // checks comparison of song votes
        song1.setVotes(5);
        song2.setVotes(10);
        song3.setVotes(8);

        event1.sortPlaylist();

        song1.setTitle("test");
        song1.setTrackUri("1");
        song1.setCoverImage("1");
        song1.setDuration(1);
        String[] artists= {"art1", "art2"};
        song1.setArtists(artists);

        Song song4= new Song("4", user.getUserId(), 1);

        //check add song if similar song should be added
        song4.setTitle("test");
        song4.setTrackUri("1");
        song4.setCoverImage("1");
        song4.setDuration(1);
        song4.setArtists(artists);

        event1.addSongToPlaylist(song4);

        //check time stamp
        JsonObject jsonObject= createEvent("megaParty", "123", 1,1,10,10,true,true ,artists);
        jsonInterpreter.handle(jsonObject);
    }

    public static JsonObject createEvent(String name, String password, double latitude, double longitude, double cooldown, double maxSongLength, boolean isExplicitAllowed, boolean isDuplicateAllowed, String[] bannedGenres) {
        JsonObject data = new JsonObject();
        data.addProperty("eventName", name);
        data.addProperty("latitude", latitude);
        data.addProperty("longitude", longitude);
        data.addProperty("addSongCooldown", cooldown);
        data.addProperty("maxSongLength", maxSongLength);
        data.addProperty("isExplicitAllowed", isExplicitAllowed);
        data.addProperty("isDuplicateAllowed", isDuplicateAllowed);

        if (password == null) data.add("password", null);
        else data.addProperty("password", password);

        JsonArray genres = new JsonArray();
        for (String s : bannedGenres) genres.add(s);
        data.add("bannedGenres", genres);

        JsonObject json = new JsonObject();

        json.addProperty("type", "createEvent");
        json.add("data", data);

        return json;
    }




}