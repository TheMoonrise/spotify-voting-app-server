package log;

//Allows commandline logging on different levels
//Log levels are defined as debug, messages, info, warning, error, off
public class Log {

    //The current log level
    private static int logLevel;

    public static void setLogLevel(String input) {

        input = input.toLowerCase();

        if (input.equals("debug")) logLevel = 0;
        if (input.equals("messages")) logLevel = 1;
        if (input.equals("info")) logLevel = 2;
        if (input.equals("warning")) logLevel = 3;
        if (input.equals("error")) logLevel = 4;
        if (input.equals("off")) logLevel = 5;
    }

    public static void d (Object sender, String log) {
        printLog(0, sender, log);
    }

    public static void m (Object sender, String log) {
        printLog(1, sender, log);
    }

    public static void i (Object sender, String log) {
        printLog(2, sender, log);
    }

    public static void w (Object sender, String log) {
        printLog(3, sender, log);
    }

    public static void e (Object sender, String log) {
        printLog(4, sender, log);
    }

    private static void printLog(int logLevel, Object sender, String log) {
        if (Log.logLevel <= logLevel) System.out.println(sender.getClass().getSimpleName() + ": " + log);
    }
}
