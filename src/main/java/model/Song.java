package model;

import java.util.ArrayList;


public class Song implements Comparable<Song> {

    private String id;
    private int votes;
    private String userId;
    private long duration;
    private String title;
    private String trackUri;
    private String coverImage;
    private String[] artists;

    private int position;
    private boolean isVotingOpen;
    private ArrayList<User> haveVoted = new ArrayList<>();

    private long commitTime;


    private boolean isExplicit;
    private boolean isVoted;

    public Song(String id, String userId, long length) {
        this.id = id;
        this.userId = userId;
        this.duration = length;
        this.votes = 0;
        this.isVotingOpen = true;
    }

    public Song(String title, String trackUri, String coverImage, long duration, boolean isExplicit, String[] artists, String songId, String userId, int position) {

        this.title = title;
        this.trackUri = trackUri;
        this.coverImage = coverImage;
        this.duration = duration;
        this.isExplicit = isExplicit;
        this.artists = artists;
        this.id = songId;
        this.votes = 0;
        this.commitTime = Long.parseLong(songId);
        this.isVoted = false;
        this.isVotingOpen = true;
        this.userId = userId;
        this.position = position;

    }

    //upvotes a Song
    public boolean upvote(User user) {
        if (!containsVote(user)) {
            votes = votes + 1;
            haveVoted.add(user);
            return true;
        }
        return false;
    }

    //downvotes a song
    public boolean downvote(User user) {
        if (!containsVote(user)) {
            votes = votes - 1;
            haveVoted.add(user);
            return true;
        }
        return false;
    }

    //HelperMethod for upvoting
    public boolean containsVote(User user) {
        for (User u : haveVoted) {
            if (user.getUserId().equalsIgnoreCase(u.getUserId())) {
                return true;
            }
        }
        return false;
    }

    //compares two songs
    public boolean equals(Song song) {

        boolean btitle = title.equalsIgnoreCase(song.getTitle());
        boolean btrackUri = trackUri.equals(song.getTrackUri());
        boolean bcoverImage = coverImage.equalsIgnoreCase(song.getCoverImage());
        boolean bduration = duration == song.getDuration();
        boolean bartists = compareArtists(song.getArtists());

        if (btitle && btrackUri && bcoverImage && bduration && bartists) {
            return true;
        }

        return false;
    }


    //compares two list of artists
    public boolean compareArtists(String[] list) {

        if (artists.length != list.length) {
            return false;
        } else {
            for (String art1 : artists) {
                for (String art2 : list) {
                    if (!art1.equalsIgnoreCase(art2)) {
                        return false;
                    }
                }
            }

            return true;
        }
    }

    public boolean hasVotedOn(User user) {

        for (User u : haveVoted) {
            if (u.getUserId().equalsIgnoreCase(user.getUserId())) {
                return true;
            }
        }
        return false;
    }

    public int compareLong(Song other) {
        if (this.getCommitTime() == other.getCommitTime()) {
            return 0;
        }
        if (this.getCommitTime() < other.getCommitTime()) {
            return -1;
        }
        return 1;

    }


    //compares the votes of the Songs (descending)
    @Override
    public int compareTo(Song song) {
        return Integer.compare(song.getVotes(), votes);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrackUri() {
        return trackUri;
    }

    public void setTrackUri(String trackUri) {
        this.trackUri = trackUri;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String[] getArtists() {
        return artists;
    }

    public void setArtists(String[] artists) {
        this.artists = artists;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isVotingOpen() {
        return isVotingOpen;
    }

    public void setVotingOpen(boolean votingOpen) {
        isVotingOpen = votingOpen;
    }

    public ArrayList<User> getHaveVoted() {
        return haveVoted;
    }

    public void setHaveVoted(ArrayList<User> haveVoted) {
        this.haveVoted = haveVoted;
    }

    public boolean isExplicit() {
        return isExplicit;
    }

    public void setExplicit(boolean explicit) {
        isExplicit = explicit;
    }

    public boolean isVoted() {
        return isVoted;
    }

    public void setVoted(boolean voted) {
        isVoted = voted;
    }

    public long getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(long commitTime) {
        this.commitTime = commitTime;
    }


}
