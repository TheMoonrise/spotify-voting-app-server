package model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Event implements Comparator<Song>{

    //The host managing this event
    private User host;


    //List of all users currently in this event
    private ArrayList<User> guests = new ArrayList<>();


    //Playlist of an Event
    private ArrayList<Song> playlist = new ArrayList<>();

    //The geographical position of this event
    private SphericalCoordinates eventPosition;

    //The password of this event
    //This is null if not required
    private String password;

    //The name this event is displayed as
    private String eventName;

    //the eventId is the mac adress of the host, so that it's a unique eventId
    // and no counter needed, that is never set back when event is deleted
    private String eventId;

    //The settings for this event
    private EventSettings settings;

    //All songs that were played on the event
    private ArrayList<Song> songHistory = new ArrayList<>();

    private long counterSongId;


    private long latestChange;


    public Event(User host, String eventName, String eventId, String password, SphericalCoordinates eventPosition, EventSettings settings) {
        this.host = host;
        this.eventName = eventName;
        this.eventId = eventId;
        this.password = password;

        this.eventPosition = eventPosition;
        this.settings = settings;
        this.counterSongId = 0;
    }

    public Event() {
    }

    //Adds a user to this event
    public void addUser(User user) {
        if (!guests.contains(user)) guests.add(user);
    }

    //Returns whether this event is password protected
    public boolean isPasswordProtected() {
        return !password.isEmpty();
    }


    //returns a specific song from playlist
    public Song getSongFromPlaylist(String songId) {
        for (Song song : playlist) {
            if (song.getId().equalsIgnoreCase(songId)) {
                return song;
            }
        }
        return null;
    }

    //adds a song to the playlist
    public void addSongToPlaylist(Song song) {
        playlist.add(song);

    }

    //compares an Event with another
    public boolean equals(Event event) {
        if (this.eventId.equalsIgnoreCase(event.getEventId())) {
            return true;
        }
        return false;
    }

    //edits the settings of an event
    public void editEvent(JsonObject data) {
        long cooldown = data.get("addSongCooldown").getAsLong();
        long length = data.get("maxSongLength").getAsLong();
        boolean explicit = data.get("isExplicitAllowed").getAsBoolean();
        boolean duplicate = data.get("isDuplicateAllowed").getAsBoolean();
        JsonArray jsonArray = data.get("bannedGenres").getAsJsonArray();
        String[] bannedGenres = new String[jsonArray.size()];

        if (jsonArray != null) {
            int len = jsonArray.size();
            for (int i = 0; i < len; i++) {
                bannedGenres[i] = (jsonArray.get(i).getAsString());
            }
        }
        settings.setAddSongCooldown(cooldown);
        settings.setMaxSongLength(length);
        settings.setExplicitAllowed(explicit);
        settings.setDuplicateAllowed(duplicate);
        settings.setBannedGenres(bannedGenres);
    }

    //sorts the playlist descending regarding the votes of the songs
    public void sortPlaylist() {
        if (!playlist.isEmpty()) {
            if (playlist.get(0).isVotingOpen()) {
                Collections.sort(playlist);
            } else {
                Song firstSong = playlist.get(0);
                playlist.remove(0);
                Collections.sort(playlist);
                playlist.add(0, firstSong);
            }
        }

    }

    //returns position of the song in the playlist
    public int getPositionOfSong(Song song) {
        int count = 0;
        for (Song s : playlist) {
            if (s.getId().equalsIgnoreCase(song.getId())) {
                return count;
            } else {
                count++;
            }
        }
        return Integer.parseInt(null);
    }

    @Override
    public int compare(Song o1, Song o2) {
        if (o1.getVotes()==o2.getVotes()){
            return o1.compareLong(o2);
        }
        return 0;
    }



    //sorts songs with same votes after their commit time
    public void sortAfterCommitTime(){
        Song firstSong;
        Comparator<Song> comp = new Event();
        if (!playlist.get(0).isVotingOpen()){
            firstSong= playlist.get(0);
            playlist.remove(0);
            Collections.sort(playlist, comp);
            playlist.add(0, firstSong);
        }else{
            Collections.sort(playlist, comp);
        }
    }


    //checks if a song is alredy in the playlist
    public boolean alreadyInPlaylist(Song song) {
        for (Song s : playlist) {
            if (s.equals(song)) {
                return true;
            }
        }
        return false;
    }

    //checks if a user is host of an event
    public boolean isHost(User user) {
        if (host.getUserId().equalsIgnoreCase(user.getUserId())) {
            return true;
        }
        return false;
    }

    public void removeSong() {
        playlist.remove(0);
    }


    public User getHost() {
        return host;
    }

    public SphericalCoordinates getEventPosition() {
        return eventPosition;
    }

    public String getPassword() {
        return password;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventId() {
        return eventId;
    }

    public EventSettings getSettings() {
        return settings;
    }

    public ArrayList<User> getGuests() {
        return guests;
    }

    public void setGuests(ArrayList<User> guests) {
        this.guests = guests;
    }

    public ArrayList<Song> getPlaylist() {
        return playlist;
    }

    public void setPlaylist(ArrayList<Song> playlist) {
        this.playlist = playlist;
    }

    public ArrayList<Song> getSongHistory() {
        return songHistory;
    }

    public void setSongHistory(ArrayList<Song> songHistory) {
        this.songHistory = songHistory;
    }

    public long getCounterSongId() {
        return counterSongId;
    }

    public void setCounterSongId(long counterSongId) {
        this.counterSongId = counterSongId;
    }

    public long getLatestChange() {
        return latestChange;
    }

    public void setLatestChange(long latestChange) {
        this.latestChange = latestChange;
    }

}
