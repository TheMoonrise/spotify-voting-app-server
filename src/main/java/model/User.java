package model;

import control.State;
import server.ClientConnection;


public class User {

    //The connection to the client
    private ClientConnection connection;

    //The state of the server
    private State state;

    //The mac address of the client
    private String userId;

    //The collected points of the user
    private int points;

    //The event the user is currently attending
    private Event event;

    //checks if client has been connected to an event before
    // in that case he isn't asked for the password anymore
    private boolean alreadyConnected;

    private boolean isHostOfEvent;

    private Event ownEvent;

    private long lastCommit;


    public User(ClientConnection connection, State state) {
        this.connection = connection;
        this.state = state;

        connection.setListener(new UserListener(this));
        this.lastCommit =0;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public ClientConnection getConnection() {
        return connection;
    }

    public void setConnection(ClientConnection connection) {
        this.connection = connection;
    }

    public State getState() {
        return state;
    }

    public boolean isAlreadyConnected() {
        return alreadyConnected;
    }

    public void setAlreadyConnected(boolean alreadyConnected) {
        this.alreadyConnected = alreadyConnected;
    }
    public void setState(State state) {
        this.state = state;
    }

    public boolean isHostOfEvent() {
        return isHostOfEvent;
    }

    public void setHostOfEvent(boolean hostOfEvent) {
        isHostOfEvent = hostOfEvent;
    }
    public Event getOwnEvent() {
        return ownEvent;
    }

    public void setOwnEvent(Event ownEvent) {
        this.ownEvent = ownEvent;
    }
    public long getLastCommit() {
        return lastCommit;
    }

    public void setLastCommit(long lastCommit) {
        this.lastCommit = lastCommit;
    }
}
