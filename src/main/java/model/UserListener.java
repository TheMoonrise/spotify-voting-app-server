package model;

import JSON.JsonInterpreter;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import log.Log;
import server.ClientConnection;

//Handles the messages coming from a specific client
public class UserListener implements ClientConnection.Listener {

    //Interpreter to handle incoming messages
    JsonInterpreter jsonInterpreter;

    //Parser to convert messages to json
    JsonParser jsonParser = new JsonParser();


    public UserListener(User user) {
        jsonInterpreter = new JsonInterpreter(user, user.getState());
    }

    @Override
    public void onMessageReceived(String message) {

        try {
            JsonObject json = jsonParser.parse(message).getAsJsonObject();
            jsonInterpreter.handle(json);

        } catch (Exception e) {
            Log.e(this, "failed to convert message to json: " + message);
        }
    }
}
