package model;

//Abstract implementation of spherical coordinates
public class SphericalCoordinates {

    //The radius of the earth
    public static final int earthRadius = 6371000;

    //The stored coordinates
    private double latitude, longitude;

    public SphericalCoordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    //Calculates the distance between two coordinates (Haversine formula)
    public double distance(double startLat, double startLon, double endLat, double endLon) {

        Double latDistance = Math.toRadians(endLat - startLat);
        Double lonDistance = Math.toRadians(endLon - startLon);

        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(startLat)) * Math.cos(Math.toRadians(endLat)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (earthRadius * c);
    }

    //Calculates the distance between two coordinates, one of them are the stored coordinates (Haversine formula)
    public double distance(double endLat, double endLon) {
        return distance(latitude, longitude, endLat, endLon);
    }

    //Calculates the distance between two coordinates (Haversine formula)
    public double distance(SphericalCoordinates coordinates) {
        return distance(latitude, longitude, coordinates.latitude, coordinates.longitude);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
