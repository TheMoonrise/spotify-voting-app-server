package model;

//Wraps the settings for an event
public class EventSettings {


    //Whether explicit songs are allowed on this event
    private boolean isExplicitAllowed;

    //The cooldown for adding songs to this event
    private long addSongCooldown;

    //The maximum length for each song of this event
    private long maxSongLength;

    //All genres banned by the host of this event
    private String[] bannedGenres;

   //checks if duplicates are allowed
    private boolean duplicateAllowed;


    public EventSettings(boolean isExplicitAllowed, long addSongCooldown, long maxSongLength, String[] bannedGenres, boolean duplicateAllowed) {
        this.isExplicitAllowed =isExplicitAllowed;
        this.addSongCooldown = addSongCooldown;
        this.maxSongLength = maxSongLength;
        this.bannedGenres = bannedGenres;
        this.duplicateAllowed= duplicateAllowed;
    }

    public boolean isExplicitAllowed() {
        return isExplicitAllowed;
    }

    public long getAddSongCooldown() {
        return addSongCooldown;
    }

    public long getMaxSongLength() {
        return maxSongLength;
    }

    public String[] getBannedGenres() {
        return bannedGenres;
    }

    public void setExplicitAllowed(boolean explicitAllowed) {
        isExplicitAllowed = explicitAllowed;
    }

    public void setAddSongCooldown(long addSongCooldown) {
        this.addSongCooldown = addSongCooldown;
    }

    public void setMaxSongLength(long maxSongLength) {
        this.maxSongLength = maxSongLength;
    }

    public void setBannedGenres(String[] bannedGenres) {
        this.bannedGenres = bannedGenres;
    }

    public boolean isDuplicateAllowed() {
        return duplicateAllowed;
    }

    public void setDuplicateAllowed(boolean duplicateAllowed) {
        this.duplicateAllowed = duplicateAllowed;
    }
}
