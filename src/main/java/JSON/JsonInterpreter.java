package JSON;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import cons.Cons;
import control.State;
import log.Log;
import model.*;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class JsonInterpreter {

    //The user attached to this interpreter
    private User user;

    //The current state of the server
    private State state;

    //The gson instance used for deserialization
    private Gson gson = new Gson();


    public JsonInterpreter(User user, State state) {
        this.state = state;
        this.user = user;
    }


    //Interprets the content of json messages
    public void handle(JsonObject jsonObject) {

        try {
            JsonObject data = jsonObject.get("data").getAsJsonObject();
            String type = jsonObject.get("type").getAsString();

            switch (type) {

                case "identifyClient":
                    identifyClient(data);
                    break;
                case "createEvent":
                    createEvent(data);
                    break;
                case "joinEvent":
                    joinEvent(data);
                    break;
                case "getNearbyEvents":
                    getNearbyEvents(data);
                    break;
                case "editEvent":
                    editEvent(data);
                    user.getEvent().setLatestChange(System.currentTimeMillis());
                    break;
                case "getCurrentEvent":
                    returnCurrentEvent();
                    break;
                case "getPlaylist":
                    returnPlaylist();
                    break;
                case "voteTrack":
                    vote(data);
                    user.getEvent().setLatestChange(System.currentTimeMillis());
                    break;
                case "addTrack":
                    addSong(createSong(data));
                    user.getEvent().setLatestChange(System.currentTimeMillis());
                    break;
                case "getCurrentlyPlaying":
                    getCurrentPlaying();
                    break;
                case "getNext":
                    sendNextSong();
                    user.getEvent().setLatestChange(System.currentTimeMillis());
                    break;
                case "getHistory":
                    sendHistory();
                    break;
                case "endEvent":
                    deleteEvent(user.getEvent().getEventId());
                    break;
                case "addTracks":
                    importPlaylist(data);
                    user.getEvent().setLatestChange(System.currentTimeMillis());
                    break;
                case "getSongAddCooldown":
                    getSongAddCooldpwn();
                    break;
            }

            removeInactiveEvents();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this, "failed to handle json: " + jsonObject.toString());
        }
    }

    //identifies client with an unique id, so that voting twice is prevented
    private void identifyClient(JsonObject data) {
        String id = data.get("userId").getAsString();
        boolean dublicate = state.checkIfIDinEvent(id, user);

        if (!dublicate) {
            user.setUserId(id);
            state.getConnectedUser().add(user);
            state.getSessions().add(user.getConnection());
        }
        user.getConnection().sendMessage(JsonBuilder.returnIdentifyClient());

    }


    //checks if a client has the right to join an event
    // if he already has joined an event which is password protected,
    // he mustn't enter the password again
    private void joinEvent(JsonObject data) {
        String eventId = data.get("eventId").getAsString();
        String password = data.get("password").getAsString();

        Event event = state.eventWithId(eventId);

        if (event == null) {
            user.getConnection().sendMessage(JsonBuilder.buildError("returnJoinEvent", "eventNotAvailable"));
            return;
        }

        if (event.isPasswordProtected() && !event.getPassword().equals(password)) {
            user.setEvent(event);
            user.getConnection().sendMessage(JsonBuilder.buildError("returnJoinEvent", "incorrectPassword"));
            return;
        } else {
            user.setEvent(event);
        }

        if (!userInEventList(event)) {
            event.addUser(user);
            event.setLatestChange(System.currentTimeMillis());
        }
        boolean host = false;
        if (event.getHost().getUserId().equalsIgnoreCase(user.getUserId())) {
            host = true;
        }
        user.getConnection().sendMessage(JsonBuilder.returnJoinEvent(event, host));
    }


    private boolean userInEventList(Event event) {
        for (User u : event.getGuests()) {
            if (u.getUserId().equalsIgnoreCase(user.getUserId())) {
                return true;
            }
        }
        return false;
    }


    //returns all events within a radius of 350m
    private void getNearbyEvents(JsonObject data) {
        double latitude = data.get("latitude").getAsDouble();
        double longitude = data.get("longitude").getAsDouble();

        SphericalCoordinates coordinates = new SphericalCoordinates(latitude, longitude);
        Event[] events = state.eventsWithinDistance(coordinates, Cons.EVENT_ACCESS_DISTANCE);

        user.getConnection().sendMessage(JsonBuilder.returnGetNearbyEvents(events));
    }


    //creates a new event
    //if the host of the event actually has another event, the old one is deleted automatically
    private void createEvent(JsonObject data) {

        if (user.getOwnEvent() != null) {
            deleteEvent(user.getOwnEvent().getEventId());
        }
        String eventName = data.get("eventName").getAsString();
        String password = data.get("password").getAsString();
        double latitude = data.get("latitude").getAsDouble();
        double longitude = data.get("longitude").getAsDouble();

        long addSongCooldown = data.get("addSongCooldown").getAsLong();
        long maxSongLength = data.get("maxSongLength").getAsLong();
        boolean isExplicitAllowed = data.get("isExplicitAllowed").getAsBoolean();
        boolean isDuplicateAllowed = data.get("isDuplicateAllowed").getAsBoolean();

        Type arrayType = new TypeToken<String[]>() {
        }.getType();
        String[] bannedGenres = gson.fromJson(data.get("bannedGenres"), arrayType);

        EventSettings settings = new EventSettings(isExplicitAllowed, addSongCooldown, maxSongLength, bannedGenres, isDuplicateAllowed);
        SphericalCoordinates eventPosition = new SphericalCoordinates(latitude, longitude);

        Event event = state.createEvent(user, eventName, password, eventPosition, settings);
        event.setLatestChange(System.currentTimeMillis());
        user.setEvent(event);
        user.setHostOfEvent(true);
        user.setOwnEvent(event);
        user.getConnection().sendMessage(JsonBuilder.returnCreateEvent(event));
    }


    //sends a message to all guests of an event
    public void sendToAllGuests(Event event, String message) {
        for (User user : event.getGuests()) {
            user.getConnection().sendMessage(message);
        }
    }


    //checks if user is allowed to vote on a song
    private void vote(JsonObject data) {
        String songId = data.get("trackId").getAsString();
        boolean isUpvote = data.get("isUpvote").getAsBoolean();

        Song song = user.getEvent().getSongFromPlaylist(songId);

        if (song.isVotingOpen()) {
            if (isUpvote) {
                if (song.upvote(user)) {
                    sendVotes(song);
                }
            } else {
                if (song.downvote(user)) {
                    sendVotes(song);
                }
            }
        }
    }


    //checks if user has still to wait until he can add a new song again
    private boolean checkCooldown() {
        long timeNow = System.currentTimeMillis();

        if (timeNow - user.getLastCommit() >= user.getEvent().getSettings().getAddSongCooldown()) {
            return true;
        }
        return false;
    }


    //informs all clients that a song has been voted
    //sorts the playlist after votes and commit time
    private void sendVotes(Song song) {
        user.getEvent().sortPlaylist();
        user.getEvent().sortAfterCommitTime();
        updateSongPositions(user.getEvent().getPlaylist(), user.getEvent());
        for (User u : user.getEvent().getGuests()) {
            String message = JsonBuilder.updateTrack(song, u);
            u.getConnection().sendMessage(message);
        }
    }

    private void updateSongPositions(ArrayList<Song> playlist, Event event) {
        for (Song song : playlist) {
            song.setPosition(event.getPositionOfSong(song));
        }
    }

    private Song createSong(JsonObject data) {

        String title = data.get("title").getAsString();
        String trackUri = data.get("trackUri").getAsString();
        String coverImage = data.get("coverImage").getAsString();
        long duration = data.get("duration").getAsLong();
        JsonArray jsonArray = data.get("artists").getAsJsonArray();
        boolean isExplicit = data.get("isExplicit").getAsBoolean();
        String[] artists = new String[jsonArray.size()];

        if (jsonArray != null) {

            artists = buildStringArray(jsonArray);
        }
        String songId = String.valueOf(user.getEvent().getCounterSongId());
        Song newSong = new Song(title, trackUri, coverImage, duration, isExplicit, artists, songId, user.getUserId(), user.getEvent().getPlaylist().size());
        newSong.setCommitTime(user.getEvent().getCounterSongId());
        return newSong;
    }


    private String[] buildStringArray(JsonArray jsonArray) {
        int len = jsonArray.size();
        String[] artists = new String[jsonArray.size()];
        for (int i = 0; i < len; i++) {

            String art1 = String.valueOf((jsonArray.get(i)));
            String art2 = "";
            int l = art1.length() - 1;
            for (int j = 1; j < l; j++) {
                art2 = art2 + art1.charAt(j);
            }

            artists[i] = art2;
        }
        return artists;
    }


    //checks if song which want to be added fulfills the settings of the event
    private boolean checkSong(Song newSong) {
        Event event = user.getEvent();

        if (!checkCooldown()) {
            return false;
        }
        if (!event.getSettings().isExplicitAllowed() && newSong.isExplicit()) {
            return false;
        }
        if (event.getSettings().getMaxSongLength() < newSong.getDuration() && event.getSettings().getMaxSongLength() != 0) {
            return false;
        }

        if (!user.getEvent().getSettings().isDuplicateAllowed() && (isDuplicate(newSong) || event.alreadyInPlaylist(newSong))) {
            return false;
        }

        return true;
    }


    //adds a new song to the playlist of an event
    private void addSong(Song newSong) {

        if (checkSong(newSong)) {
            long time = System.currentTimeMillis();
            user.setLastCommit(time);
            sendNewSong(newSong);
        } else {
            user.getConnection().sendMessage(JsonBuilder.returnAddTrack(false));
        }
    }


    public void sendNewSong(Song newSong) {
        user.getEvent().addSongToPlaylist(newSong);
        user.getEvent().sortPlaylist();
        newSong.setPosition(user.getEvent().getPositionOfSong(newSong));
        user.getEvent().setCounterSongId(user.getEvent().getCounterSongId() + 1);
        for (User u : user.getEvent().getGuests()) {
            String message1 = JsonBuilder.addTrack(newSong, u);
            u.getConnection().sendMessage(message1);
        }
        user.getConnection().sendMessage(JsonBuilder.returnAddTrack(true));

    }


    //impots the Spotify playlist of an user
    private void importPlaylist(JsonObject data) {
        JsonArray jsonArray = data.get("tracks").getAsJsonArray();


        if (jsonArray != null) {
            int len = jsonArray.size();

            for (int i = 0; i < len; i++) {
                JsonObject track = (JsonObject) jsonArray.get(i);
                String title = track.get("title").getAsString();
                String trackUri = track.get("trackUri").getAsString();
                String coverImage = track.get("coverImage").getAsString();
                long duration = track.get("duration").getAsLong();
                JsonArray artistsJSON = track.get("artists").getAsJsonArray();
                boolean isExplicit = track.get("isExplicit").getAsBoolean();
                String artists[] = new String[artistsJSON.size()];

                if (artistsJSON != null) {
                    artists = buildStringArray(artistsJSON);
                }

                String songId = String.valueOf(user.getEvent().getCounterSongId());
                Song song = new Song(title, trackUri, coverImage, duration, isExplicit, artists, songId, user.getUserId(), user.getEvent().getPlaylist().size());
                song.setCommitTime(user.getEvent().getCounterSongId());

                user.getEvent().getPlaylist().add(song);
                user.getEvent().setCounterSongId(user.getEvent().getCounterSongId() + 1);

                for (User u : user.getEvent().getGuests()) {
                    String message1 = JsonBuilder.addTrack(song, u);
                    u.getConnection().sendMessage(message1);
                }
                user.getConnection().sendMessage(JsonBuilder.returnAddTrack(true));

            }

        }
    }


    //checks if a song already has been played on an event
    public boolean isDuplicate(Song song) {
        ArrayList<Song> songHistory = user.getEvent().getSongHistory();

        for (Song s : songHistory) {
            if (s.equals(song)) {
                return true;
            }
        }
        return false;
    }


    //sends the next song in line to the event
    private void sendNextSong() {
        if (user.getEvent().isHost(user)) {
            Event event = user.getEvent();
            if (event.getPlaylist().size() > 1) {
                Song nextSong = event.getPlaylist().get(1);
                user.getConnection().sendMessage(JsonBuilder.returnGetNext(nextSong, user));
                removeSong(event);

                nextSong.setVotingOpen(false);
                nextSong.setPosition(user.getEvent().getPositionOfSong(nextSong));

                for (User u : user.getEvent().getGuests()) {
                    String message = JsonBuilder.updateTrack(nextSong, u);
                    u.getConnection().sendMessage(message);
                }
            } else {
                if (event.getPlaylist().size() == 1) {
                    removeSong(event);
                }
                user.getConnection().sendMessage(JsonBuilder.returnGetNext(null, user));
            }
        }
    }


    //helper method for sendNextSong()
    public void removeSong(Event event) {
        Song currentSong = event.getPlaylist().get(0);
        event.getSongHistory().add(currentSong);
        event.removeSong();
        sendToAllGuests(user.getEvent(), JsonBuilder.removetrack(currentSong.getId()));
    }


    //sends the song which is playing at the moment
    private void getCurrentPlaying() {
        Event event = user.getEvent();
        if (!event.getPlaylist().isEmpty() && event != null) {
            if (event.isHost(user)) {
                user.getConnection().sendMessage(JsonBuilder.returnGetCurrentlyPlaying(event, user));
                Song currentSong = event.getPlaylist().get(0);
                currentSong.setVotingOpen(false);
                for (User u : user.getEvent().getGuests()) {
                    String message = JsonBuilder.updateTrack(currentSong, u);
                    u.getConnection().sendMessage(message);
                }
            }
        }
    }


    private void returnCurrentEvent() {
        if (eventAvailable()) {
            user.getConnection().sendMessage(JsonBuilder.updateEvent(user.getEvent()));
        } else {
            user.getConnection().sendMessage(JsonBuilder.buildError("updateEvent", "eventNotAvailable"));
        }

    }


    //edits the settings of an event
    private void editEvent(JsonObject data) {
        if (eventAvailable()) {
            user.getEvent().editEvent(data);
            sendToAllGuests(user.getEvent(), JsonBuilder.updateEvent(user.getEvent()));
        } else {
            user.getConnection().sendMessage(JsonBuilder.buildError("updateEvent", "eventNotAvailable"));
        }

    }

    private boolean eventAvailable() {
        Event userEvent = user.getEvent();
        for (Event event : state.getEvents()) {
            if (event.getEventId().equalsIgnoreCase(userEvent.getEventId())) {
                return true;
            }
        }
        return false;
    }


    //sends all songs which have already been played in an event
    private void sendHistory() {
        user.getConnection().sendMessage(JsonBuilder.returnGetHistory(user.getEvent(), user));
    }

    private void returnPlaylist() {
        String message = JsonBuilder.returnPlaylist(user.getEvent(), user);
        user.getConnection().sendMessage(message);
    }

    private void deleteEvent(String eventId) {

        state.deleteEvent(eventId);
        user.setHostOfEvent(false);
    }


    private void getSongAddCooldpwn() {
        String message = JsonBuilder.sendOwnCooldown(user.getLastCommit(), user.getEvent().getSettings().getAddSongCooldown());
        user.getConnection().sendMessage(message);
    }

    //removes an totally inactive event after three hours
    private void removeInactiveEvents() {
        long threeHours = System.currentTimeMillis() - 10800000;
        for (Event event : state.getEvents()) {
            if (event.getLatestChange() <= threeHours) {
                deleteEvent(event.getEventId());
            }
        }
    }


    public void setUser(User user) {
        this.user = user;
    }
}
