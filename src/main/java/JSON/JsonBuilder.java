package JSON;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import model.Event;

import model.Song;
import model.User;

public abstract class JsonBuilder {

    //this class builds all the Json messages which are sent to the client

    //Finalizes a message to be sent to a client
    public static String buildMessage(String type, JsonObject data) {
        JsonObject json = new JsonObject();
        json.addProperty("type", type);
        json.addProperty("error", false);

        json.add("data", data);
        return json.toString();
    }

    //Finalizes an error message sent to a client
    public static String buildError(String type, String errorMessage) {
        JsonObject json = new JsonObject();
        JsonObject data = new JsonObject();

        json.addProperty("type", type);
        json.addProperty("error", true);
        data.addProperty("errorMessage", errorMessage);

        json.add("data", data);
        return json.toString();
    }

    //Creates a json representation from an event
    public static JsonObject eventJson(Event event) {
        JsonObject eventJson = new JsonObject();
        eventJson.addProperty("eventId", event.getEventId());
        eventJson.addProperty("eventPassword", event.getPassword());
        eventJson.addProperty("eventName", event.getEventName());
        eventJson.addProperty("isPasswordProtected", event.isPasswordProtected());
        eventJson.addProperty("addSongCooldown", event.getSettings().getAddSongCooldown());
        eventJson.addProperty("isExplicitAllowed", event.getSettings().isExplicitAllowed());
        eventJson.addProperty("maxSongLength", event.getSettings().getMaxSongLength());
        eventJson.addProperty("isDuplicateAllowed", event.getSettings().isDuplicateAllowed());

        JsonArray bannedGenres = new JsonArray();
        for (String g : event.getSettings().getBannedGenres()) bannedGenres.add(g);
        eventJson.add("bannedGenres", bannedGenres);

        return eventJson;
    }

    // Creates a json representation of a Song
    public static JsonObject songJson(Song song, User user) {
        JsonObject songJson = new JsonObject();

        if (song != null) {
            songJson.addProperty("trackId", song.getId());
            songJson.addProperty("title", song.getTitle());
            songJson.addProperty("trackUri", song.getTrackUri());
            songJson.addProperty("coverImage", song.getCoverImage());
            songJson.addProperty("duration", song.getDuration());
            songJson.addProperty("votes", song.getVotes());
            songJson.addProperty("position", song.getPosition());
            songJson.addProperty("isExplicit", song.isExplicit());
            songJson.addProperty("isVotingOpen", song.isVotingOpen());

            JsonArray artists = new JsonArray();

            if (song.getArtists() != null) {
                for (String a : song.getArtists()) {
                    artists.add(a);
                }
            }
            songJson.add("artists", artists);

            boolean isVoted = song.hasVotedOn(user);
            songJson.addProperty("isVoted", isVoted);

        }
        return songJson;
    }


    public static String returnIdentifyClient() {
        return buildMessage("returnIdentifyClient", new JsonObject());
    }

    public static String returnGetNearbyEvents(Event[] events) {
        JsonObject data = new JsonObject();
        JsonArray eventsJson = new JsonArray();

        for (Event e : events) eventsJson.add(eventJson(e));
        data.add("events", eventsJson);
        return buildMessage("returnGetNearbyEvents", data);
    }

    public static String returnJoinEvent(Event event, boolean isHost) {
        JsonObject data = new JsonObject();
        JsonObject eventJson = eventJson(event);

        data.add("event", eventJson);
        data.addProperty("isHost", isHost);
        return buildMessage("returnJoinEvent", data);
    }

    public static String returnCreateEvent(Event event) {
        JsonObject data = new JsonObject();
        JsonObject eventJson = eventJson(event);

        data.add("event", eventJson);
        return buildMessage("returnCreateEvent", data);
    }

    //tells client that it is not necessary to asked for the password again
    public static String wasAlredyConnected() {
        return buildMessage("wasAlredyConnectet", new JsonObject());
    }

    public static String updateEvent(Event event) {

        JsonObject data = new JsonObject();
        JsonObject eventJson = eventJson(event);

        data.add("event", eventJson);
        return buildMessage("updateEvent", data);

    }

    public static String returnPlaylist(Event event, User user) {

        JsonObject data = new JsonObject();
        JsonArray playlist = new JsonArray();

        if (event!=null && !event.getPlaylist().isEmpty()) {
            for (Song song : event.getPlaylist()) {
                JsonObject songJson = songJson(song, user);
                playlist.add(songJson);
            }
        }
        data.add("playlist", playlist);
        return buildMessage("returnGetPlaylist", data);

    }

    public static String updateTrack(Song song, User user) {

        JsonObject data = new JsonObject();
        JsonObject songJson = songJson(song, user);
        data.add("track", songJson);

        return buildMessage("updateTrack", data);

    }

    public static String addTrack(Song song, User user) {
        JsonObject data = new JsonObject();
        JsonObject songJson = songJson(song, user);
        data.add("track", songJson);

        return buildMessage("addTrack", data);

    }

    public static String removetrack(String songId) {
        JsonObject data = new JsonObject();
        data.addProperty("trackId", songId);

        return buildMessage("removeTrack", data);

    }

    public static String returnGetCurrentlyPlaying(Event event, User user) {
        JsonObject data = new JsonObject();

        if (!event.getPlaylist().isEmpty()) {
            Song song = event.getPlaylist().get(0);
            JsonObject songJson = songJson(song, user);
            data.add("track", songJson);
        }

        return buildMessage("returnGetCurrentlyPlaying", data);
    }

    public static String returnGetNext(Song song, User user) {
        JsonObject data = new JsonObject();
        if (song != null) {
            JsonObject songJson = songJson(song, user);
            data.add("track", songJson);
            return buildMessage("returnGetNext", data);
        } else {
            return buildError("returnGetNext", "trackNotAvailable");
        }

    }

    public static String returnGetHistory(Event event, User user) {
        JsonObject data = new JsonObject();

        JsonArray songHistory = new JsonArray();
        for (Song song : event.getSongHistory()) {
            JsonObject songJson = songJson(song, user);
            songHistory.add(songJson);
        }

        data.add("history", songHistory);
        return buildMessage("returnGetHistory ", data);

    }

    public static String updatePlaylist(Event event) {

        JsonObject data = new JsonObject();
        JsonArray playlist = new JsonArray();

        for (Song song : event.getPlaylist()) {
            playlist.add(String.valueOf(song));
        }
        data.add("playlist", playlist);
        return buildMessage("updatePlaylist", data);

    }

    public static String returnAddTrack(boolean added) {
        JsonObject data = new JsonObject();
        if (added) {
            return buildMessage("returnAddTrack", data);
        } else {
            return buildError("returnAddTrack", "trackNotAllowed");
        }
    }

    public static String eventEnded(Event event) {
        JsonObject data = new JsonObject();
        JsonObject eventJson = eventJson(event);

        data.add("eventEnded", eventJson);
        return buildMessage("eventEnded", data);
    }

    public static String sendOwnCooldown(long lastVote, long cooldown){
        JsonObject data = new JsonObject();

        data.addProperty("lastVote", lastVote);
        data.addProperty("cooldown", cooldown);
        return buildMessage("returnGetSongAddCooldown", data);
    }

}
